import { connect } from 'react-redux';
import { Navigate, Outlet } from 'react-router-dom';

const AppShell = ({isAuthenticated}) => {
    // console.log("isAuthenticated", isAuthenticated(), state)
    console.log(isAuthenticated);
    if (isAuthenticated) {
        return (
          <>
            <Outlet />
          </>
        );
      }
    
      return <Navigate to="/login" />;
};

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
  });
export default connect(mapStateToProps)(AppShell);
