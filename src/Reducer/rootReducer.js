import { combineReducers } from 'redux';
import counterReducer from './counterReducer';
import authReducer from './authReducer';
// import userReducer from './userReducer';

const rootReducer = combineReducers({
  counter: counterReducer,
  auth: authReducer,
});

export default rootReducer;
