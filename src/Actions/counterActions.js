// actions/counterActions.js

export const incrementCounter = (payload) => {
    return {
      type: 'INCREMENT',
    };
  };
  
  export const decrementCounter = () => {
    return {
      type: 'DECREMENT',
    };
  };
  
  // actions.js
export const login = () => ({
  type: 'LOGIN',
});

export const logout = () => ({
  type: 'LOGOUT',
});

export const setUser = (user) => ({
  type: 'SET_USER',
  payload: user,
});

