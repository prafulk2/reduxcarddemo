import { all } from 'redux-saga/effects';
import { watchIncrementCounter } from './counterSaga';

export default function* rootSaga() {
  yield all([watchIncrementCounter()]);
}
