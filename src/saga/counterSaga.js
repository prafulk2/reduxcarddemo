import { put, takeEvery, select } from 'redux-saga/effects';
import { incrementCounterSuccess, incrementCounterFailure } from '../Actions/counterActions';

function* incrementCounterSaga() {
  try {
    const counter = yield select((state) => state.counter);

    if (counter.count >= 10) {
      console.log('Counter is already at its maximum value');
      return;
    }

    yield put(incrementCounterSuccess());
  } catch (error) {
    yield put(incrementCounterFailure(error));
  }
}

export function* watchIncrementCounter() {
  yield takeEvery('INCREMENT', incrementCounterSaga);
}
