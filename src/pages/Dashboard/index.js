import React from 'react'
import { incrementCounter } from '../../Actions/counterActions'
import { connect } from 'react-redux'
import { Button } from 'antd'


 function Dashboard({data,dispatch}) {
    const updateCounter= ()=>{
        dispatch(incrementCounter(7))
      }
  return (
    <div>
        <h3>Counter:{data.counter.count}</h3>
        <button onClick={updateCounter}>update counter</button>
        <Button type="primary">Button</Button>
    </div>
  )
}
const mapStateToProps = (state) => {
    return {
      data: state,
    };
  };
  export default connect(mapStateToProps)(Dashboard);

