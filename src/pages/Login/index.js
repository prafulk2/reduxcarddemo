import { Button, Checkbox, Col, Form, Input, Row } from 'antd';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import React from 'react'
import { login } from '../../Actions/counterActions';
import { connect } from 'react-redux';

function Login({dispatch}) {
  console.log(dispatch);
  const onFinish = (values) => {
    const { email, password } = values;
    console.log(email);
    if (email !== 'abc') {
      handleEmailValidationError('Your Email is not found');
      return;
    }
    if (password !== 'abcd') {
      handlePasswordValidationError('Your password is invalid');
      return;
    }
    handleFieldChange()
    dispatch(login())
    console.log('Form submitted:', values);
    localStorage.setItem('token',true)
    form.resetFields()
  };

  const handleEmailValidationError = (errorMessage) => {
    form.setFields([
      {
        name: 'email',
        errors: [errorMessage],
      },
    ]);
  };

  const handlePasswordValidationError = (errorMessage) => {
    form.setFields([
      {
        name: 'password',
        errors: [errorMessage],
      },
    ]);
  };

  const handleFieldChange = () => {
    form.validateFields(['email', 'password']); 
  };  
  const [form] = Form.useForm();

  return (
    <Row justify='center'>
      <Col span={12}>
      <Form
      name="normal_login"
      className="login-form"
      form={form}
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        rules={[
          {
            required: true,
            message: 'Please enter your email',
          },
        ]}
        help={form.getFieldError('email')?.[0]}
      >
        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: 'Please enter your password',
          },
        ]}
        help={form.getFieldError('password')?.[0]}
      >
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Log in
        </Button>
      </Form.Item>
    </Form>
      </Col>
    </Row>
  );
}
export default connect()(Login);